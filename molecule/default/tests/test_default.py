import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_sudoers_files(host):
    assert host.file("/etc/sudoers.d/test1").content_string.strip() == "test1 ALL=(ALL) NOPASSWD:ALL"
    assert host.file("/etc/sudoers.d/test2").content_string.strip() == "test2 ALL=(ALL) ALL"
    assert host.file("/etc/sudoers.d/test3").content_string.strip() == "test3 ALL=(ALL) ALL"
    assert host.file("/etc/sudoers.d/admin_users").content_string == "test4 ALL=(ALL) NOPASSWD:ALL\ntest5 ALL=(ALL) ALL\ntest6 ALL=(ALL) ALL\ntest7 ALL=(ALL) ALL\n"


def test_sudo_with_users(host):
    for user in ['test1', 'test4']:
        with host.sudo(user):
            cmd = host.run("sudo ls")
        assert cmd.rc == 0
