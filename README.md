# ics-ans-role-sudoers

Ansible role to manage sudoers

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Role Variables

```yaml
sudoers_user_list: []
# Should be a list of dict (with user name)
# nopasswd can be omitted (default to false)
# sudoers_user_list:
#   - name: test1
#     nopasswd: true
#   - name: test2
#     nopasswd: false
#   - name: test3
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-sudoers
```

## License

BSD 2-clause
